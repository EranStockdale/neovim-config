-- Options
vim.opt.number = true
vim.opt.signcolumn = "number"

-- Bootstrap lazy.nvim
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", -- latest stable release
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)

-- Setup lazy.nvim
require('lazy').setup({
	"folke/lazy.nvim",

	{ "catppuccin/nvim", name = "catppuccin", priority = 1000 },

	{ "williamboman/mason.nvim", opts = { ensure_installed = { "clangd" } } },
	"williamboman/mason-lspconfig.nvim",
	"neovim/nvim-lspconfig",
})

-- Setup Plugins
require("catppuccin").setup({
	flavour = "mocha"
})
vim.cmd.colorscheme "catppuccin"

require("mason").setup()
require("mason-lspconfig").setup {
	ensure_installed = {
		"clangd",
	}
}
require('lspconfig').clangd.setup {}
